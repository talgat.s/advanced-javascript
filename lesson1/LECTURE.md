# Lesson 1: Lexical Environment & Execution Context

## Theory

### Syntax Parser

A program which reads and "understands" your code.

![parcer cycle](v8_parser_cycle.png)

_More_: The Journey of JavaScript [part1](https://www.telerik.com/blogs/journey-of-javascript-downloading-scripts-to-execution-part-i), [part2](https://www.telerik.com/blogs/journey-of-javascript-downloading-scripts-to-execution-part-ii), [part3](https://www.telerik.com/blogs/the-journey-of-javascript-from-downloading-scripts-to-execution-part-iii)

### Lexical Environment

Data structure that holds identifier-variable mapping.

```js
let language = 'JavaScript';
function a() {
  const b = "Dart";
  console.log("Inside function a");
}
a();
```

Global lexical environment:
```json
{
  environmentRecord: {
      language    : 'JavaScript',
      a : < reference to function object >,
  }
  outer: null,
}
```

a lexical environment:
```json
{
  environmentRecord: {
    b    : 'Dart',
  }
  outer: <globalLexicalEnvironment>
}
```

### Variable Environment

Almost the same thing as Lexical Environment.

In ES6, one difference between **LexicalEnvironment** component and the **VariableEnvironment** component is that the former is used to store function declaration and variable (`let` and `const`) bindings, while the latter is used to store the variable (`var`) bindings only.

### Reference to the Outer Environment

We need reference for outer/parent environment because code might not find reference to a variable in a current lexical environment. So it will search in outer ones([Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)). We will discuss this concept in the future.

### Context (This)

In the global execution context, the value of this refers to the global object. (in browsers, this refers to the Window Object).

In the function execution context, the value of this depends on how the function is called. Or created for arrow functions. We will discuss this concept in the future.

### Execution context

A closest parent container of a code. 

Types:
* **Global Execution Context**: This is the default or base execution context. The code that is not inside any function is in the global execution context. It performs two things: it creates a global object which is a window object (in the case of browsers) and sets the value of this to equal to the global object. There can only be one global execution context in a program.
* **Functional Execution Context**: Every time a function is invoked, a brand-new execution context is created for that function. Each function has its own execution context, but it’s created when the function is invoked or called. There can be any number of function execution contexts. Whenever a new execution context is created, it goes through a series of steps in a defined order which I will discuss later in this article.
* **Eval**: Never use it!!!

### Execution Stack

Or **"calling stack"** is a stack([LIFO](https://en.wikipedia.org/wiki/Stack_(abstract_data_type))) of all execution contexts created during execution of a code.

```js
let a = 'Hello World!';

function first() {
  console.log('Inside first function');
  second();
  console.log('Again inside first function');
}

function second() {
  console.log('Inside second function');
}

first();
console.log('Inside Global Execution Context');
```

![execution_stack](execution_stack.webp)

### The Creation Phase

The execution context is created during the creation phase.

```js
ExecutionContext = {
  LexicalEnvironment = <ref. to LexicalEnvironment in memory>,
  VariableEnvironment = <ref. to VariableEnvironment in  memory>,
}
```

### The Execution Phase

The state of during code execution.

## Example:

```js
let a = 20;
const b = 30;
var c;

function multiply(e, f) {
 var g = 20;
 return e * f * g;
}

c = multiply(20, 30);
```

### The Creation Phase: Global

```json
GlobalExectionContext = {
    LexicalEnvironment: {
        EnvironmentRecord: {
            Type: "Object",
            // Identifier bindings go here
            a: < uninitialized >,
            b: < uninitialized >,
            multiply: < func >
        }
        outer: <null>,
        ThisBinding: <Global Object>
    },
    VariableEnvironment: {
        EnvironmentRecord: {
            Type: "Object",
            // Identifier bindings go here
            c: undefined,
        }
        outer: <null>,
        ThisBinding: <Global Object>
    }
}
```

### The Execution Phase: Global

```json
GlobalExectionContext = {
    LexicalEnvironment: {
        EnvironmentRecord: {
            Type: "Object",
            // Identifier bindings go here
            a: 20,
            b: 30,
            multiply: < func >
        },
        outer: <null>,
        ThisBinding: <Global Object>
    },
    VariableEnvironment: {
        EnvironmentRecord: {
            Type: "Object",
            // Identifier bindings go here
            c: undefined,
        },
        outer: <null>,
        ThisBinding: <Global Object>
    }
}
```

### The Creation Phase: multiply

```json
FunctionExectionContext = {
    LexicalEnvironment: {
        EnvironmentRecord: {
            Type: "Declarative",
            // Identifier bindings go here
            Arguments: {0: 20, 1: 30, length: 2},
        },
        outer: <GlobalLexicalEnvironment>,
        ThisBinding: <Global Object or undefined>,
    },
    VariableEnvironment: {
        EnvironmentRecord: {
            Type: "Declarative",
            // Identifier bindings go here
            g: undefined
        },
        outer: <GlobalLexicalEnvironment>,
        ThisBinding: <Global Object or undefined>
    }
}
```

### The Execution Phase: multiply

```json
FunctionExectionContext = {
    LexicalEnvironment: {
        EnvironmentRecord: {
        Type: "Declarative",
        // Identifier bindings go here
        Arguments: {0: 20, 1: 30, length: 2},
        },
        outer: <GlobalLexicalEnvironment>,
        ThisBinding: <Global Object or undefined>,
    },
    VariableEnvironment: {
        EnvironmentRecord: {
            Type: "Declarative",
            // Identifier bindings go here
            g: 20
        },
        outer: <GlobalLexicalEnvironment>,
        ThisBinding: <Global Object or undefined>
    }
}
```


